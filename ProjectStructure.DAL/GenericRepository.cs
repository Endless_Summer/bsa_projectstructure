﻿using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL
{
    class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, IEntity
    {
        private readonly List<TEntity> _context;

        public GenericRepository(List<TEntity> context)
        {
            _context = context;
        }

        public void Create(TEntity item)
        {
            _context.Add(item);
        }

        public TEntity FindById(int id)
        {
            return _context.Find(e => e.Id == id);
        }

        public IEnumerable<TEntity> Get()
        {
            return _context.ToList();
        }

        public IEnumerable<TEntity> Get(Func<TEntity, bool> predicate)
        {
            return _context.Where(predicate).ToList();
        }

        public void Remove(TEntity item)
        {
            _context.Remove(item);
        }

        public void Update(TEntity item)
        {
            var entity = _context.Find(e => e.Id == item.Id);
            int index = _context.IndexOf(entity);

            _context[index] = item;
        }
    }

}

