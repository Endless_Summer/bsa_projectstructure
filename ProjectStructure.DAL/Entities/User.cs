﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public ICollection<UserTeam> UserTeams { get; internal set; }
        public Team Team { get; set; }

    }
}
