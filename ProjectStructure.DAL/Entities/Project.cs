﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Entities
{
    public class Project : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public List<Task> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }

    }
}
