﻿
namespace ProjectStructure.DAL.Entities
{
    public class TaskState : IEntity
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
