﻿namespace ProjectStructure.DAL.Entities
{
    public class UserInfo
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TasksLastProject { get; set; }
        public int NotComletedTasks { get; set; }
        public Task MaxTask { get; set; }
    }
}
