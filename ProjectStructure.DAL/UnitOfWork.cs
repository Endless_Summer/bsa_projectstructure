﻿using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        PsContext context = new PsContext();

        IGenericRepository<User> userRepository;
        IGenericRepository<Team> teamRepository;
        IGenericRepository<TaskState> taskStateRepository;
        IGenericRepository<Task> taskRepository;
        IGenericRepository<Project> projectRepository;

        public IGenericRepository<User> Users
        {
            get
            {
                if (userRepository == null)
                    userRepository = new GenericRepository<User>(context.Users);
                return userRepository;
            }
        }

        public IGenericRepository<Team> Teams
        {
            get
            {
                if (teamRepository == null)
                    teamRepository = new GenericRepository<Team>(context.Teams);
                return teamRepository;
            }
        }

        public IGenericRepository<TaskState> TaskStates
        {
            get
            {
                if (taskStateRepository == null)
                    taskStateRepository = new GenericRepository<TaskState>(context.TaskStates);
                return taskStateRepository;
            }
        }

        public IGenericRepository<Task> Tasks
        {
            get
            {
                if (taskRepository == null)
                    taskRepository = new GenericRepository<Task>(context.Tasks);
                return taskRepository;
            }
        }

        public IGenericRepository<Project> Projects
        {
            get
            {
                if (projectRepository == null)
                    projectRepository = new GenericRepository<Project>(context.Projects);
                return projectRepository;
            }
        }
    }
}
