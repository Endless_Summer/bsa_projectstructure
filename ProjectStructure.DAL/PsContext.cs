﻿using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL
{
    public class PsContext
    {
        public PsContext()
        {
            GenerateDB();
        }
        public List<User> Users { get; set; }
        public List<Team> Teams { get; set; }
        public List<Task> Tasks { get; set; }
        public List<TaskState> TaskStates { get; set; }
        public List<Project> Projects { get; set; }

        private void GenerateDB()
        {
            Team t1 = new Team { Id = 1, Name = "debitis", CreatedAt = DateTime.Parse("2020-06-30T20:20:46.3968861+00:00") };
            Team t2 = new Team { Id = 2, Name = "asperiores", CreatedAt = DateTime.Parse("2020-06-30T18:46:57.0010953+00:00") };
            Team t3 = new Team { Id = 3, Name = "cupiditate", CreatedAt = DateTime.Parse("2020-07-01T03:02:23.1538591+00:00") };

            Teams = new List<Team> { t1, t2, t3 };
            // ==========================================================================
            TaskState ts1 = new TaskState { Id = 0, Value = "Created" };
            TaskState ts2 = new TaskState { Id = 1, Value = "Started" };
            TaskState ts3 = new TaskState { Id = 2, Value = "Finished" };
            TaskState ts4 = new TaskState { Id = 3, Value = "Canceled" };

            TaskStates = new List<TaskState> { ts1, ts2, ts3, ts4 };
            // ==========================================================================
            User u1 = new User { Id = 1, FirstName = "Jordane", LastName = "Walker", Email = "Jordane.Walker@gmail.com", Birthday = DateTime.Parse("2004-10-28T21:26:53.0193606+00:00"), RegisteredAt = DateTime.Parse("2020-04-24T09:11:10.5691173+00:00"), Team = t1 };
            User u2 = new User { Id = 2, FirstName = "Kolby", LastName = "Jones", Email = "Kolby_Jones@yahoo.com", Birthday = DateTime.Parse("2005-10-28T21:26:53.0193606+00:00"), RegisteredAt = DateTime.Parse("2020-06-24T09:11:10.5691173+00:00"), Team = t2 };
            User u3 = new User { Id = 3, FirstName = "BMabelle", LastName = "Miller", Email = "Mabelle.Miller12@hotmail.com", Birthday = DateTime.Parse("2015-09-29T15:28:43.7369629+00:00"), RegisteredAt = DateTime.Parse("2020-06-22T12:35:42.5834106+00:00"), Team = t3 };
            User u4 = new User { Id = 4, FirstName = "Verla", LastName = "Bechtelar", Email = "Verla.Bechtelar62@gmail.com", Birthday = DateTime.Parse("2008-07-14T20:47:23.7087566+00:00"), RegisteredAt = DateTime.Parse("2020-07-01T05:22:15.869556+00:00"), Team = null };
            User u5 = new User { Id = 5, FirstName = "ARetha", LastName = "Will", Email = "Retha67@yahoo.com", Birthday = DateTime.Parse("2007-09-20T10:05:41.1527935+00:00"), RegisteredAt = DateTime.Parse("2020-06-23T18:09:18.5038054+00:00"), Team = t1 };

            Users = new List<User> { u1, u2, u3, u4, u5 };
            // ==========================================================================
            Project p1 = new Project { Id = 1, Name = "Expedita amet quas id a.", Description = "Ea ab omnis saepe rem vel et.\nIllo quaerat eos accusantium reiciendis dolores quibusdam ratione.", CreatedAt = DateTime.Parse("2020-08-01T03:15:53.2885115+00:00"), Deadline = DateTime.Parse("2021-02-02T00:57:55.0677911+00:00"), Author = u1, Team = t1 };
            Project p2 = new Project { Id = 2, Name = "Totam autem hic atque suscipit.", Description = "Aut quia id adipisci alias non mollitia.\nAlias et at quia soluta quisquam aspernatur nemo molestias.\nVel id suscipit vero ipsa repudiandae nesciunt.\nProvident veritatis maiores aut.\nIste et incidunt.", CreatedAt = DateTime.Parse("2020-06-30T23:19:09.5122629+00:00"), Deadline = DateTime.Parse("2021-02-02T00:57:55.0677911+00:00"), Author = u2, Team = t2 };
            Project p3 = new Project { Id = 3, Name = "Eos illum eum minima quibusdam.", Description = "Quis dicta repudiandae consequatur et odio repudiandae occaecati.\nDolore fugit veniam dolorem aperiam consequatur cum sed officiis ut.\nExercitationem ea ducimus saepe id asperiores dignissimos molestiae repellat.", CreatedAt = DateTime.Parse("2020-07-01T12:59:04.683596+00:00"), Deadline = DateTime.Parse("2020-11-07T12:49:38.5028811+00:00"), Author = u1, Team = t1 };

            Projects = new List<Project> { p1, p2, p3 };
            // ==========================================================================
            Task ta1 = new Task { Id = 1, Name = "Quasi consectetur nesciunt doloribus.", Description = "Praesentium autem consequatur magnam et doloribus exercitationem.\nAut animi fuga cupiditate debitis atque nisi consequatur consequatur.\nCupiditate necessitatibus quo eos sequi earum et quis accusamus.", CreatedAt = DateTime.Parse("2020-06-30T22:22:09.9030937+00:00"), FinishedAt = DateTime.Parse("2020-12-10T22:30:51.0724501+00:00"), State = 2, Project = p1, Performer = u1 };
            Task ta2 = new Task { Id = 2, Name = "Praesentium ut consequatur cumque eveniet suscipit amet officia.", Description = "Sed voluptas quia dolores expedita eius laborum ut qui aspernatur.\nMolestias sapiente pariatur fuga architecto sed.\nAutem repellendus maxime magni qui exercitationem rerum.\nDolorem magnam aut commodi nemo aut quaerat.\nEos sit veniam qui molestiae facere voluptatem.\nFacilis eum atque enim dolor facilis ea ipsum tempora.", CreatedAt = DateTime.Parse("2020-07-01T11:49:39.729857+00:00"), FinishedAt = DateTime.Parse("2020-07-19T22:05:30.3390708+00:00"), State = 0, Project = p2, Performer = u2 };
            Task ta3 = new Task { Id = 3, Name = "Error sit sunt.", Description = "Praesentium autem consequatur magnam et doloribus exercitationem.\nAut animi fuga cupiditate debitis atque nisi consequatur consequatur.\nCupiditate necessitatibus quo eos sequi earum et quis accusamus.", CreatedAt = DateTime.Parse("2020-06-30T22:52:33.0645959+00:00"), FinishedAt = DateTime.Parse("2020-07-25T07:38:48.6570938+00:00"), State = 3, Project = p3, Performer = u3 };
            Task ta4 = new Task { Id = 4, Name = "Repellendus itaque expedita est ut.", Description = "Nisi esse accusamus dolorem blanditiis porro est dolores.\nExplicabo consequatur rem dignissimos odit praesentium.\nMolestiae facilis et tenetur.\nVoluptas quis sed et ab nulla omnis cupiditate.\nId sed et.", CreatedAt = DateTime.Parse("2020-07-01T01:35:10.4670552+00:00"), FinishedAt = DateTime.Parse("2020-10-19T05:59:38.2813374+00:00"), State = 2, Project = p1, Performer = u2 };
            Task ta5 = new Task { Id = 5, Name = "V", Description = "Rerum totam sit.\nVelit saepe iusto et repellat et consequuntur sit.\nVoluptate officiis pariatur ut ea.\nNeque ut sed voluptatem occaecati.\nDolor velit quaerat molestiae assumenda veritatis voluptatem.", CreatedAt = DateTime.Parse("2020-06-30T19:51:23.2733812+00:00"), FinishedAt = DateTime.Parse("2021-06-20T02:07:03.7230486+00:00"), State = 1, Project = p2, Performer = u5 };

            Task ta6 = new Task { Id = 6, Name = "Quasi consectetur nesciunt doloribus.Quasi consectetur nesciunt doloribus.", Description = "Praesentium autem consequatur magnam et doloribus exercitationem.\nAut animi fuga cupiditate debitis atque nisi consequatur consequatur.\nCupiditate necessitatibus quo eos sequi earum et quis accusamus.", CreatedAt = DateTime.Parse("2020-06-30T22:22:09.9030937+00:00"), FinishedAt = DateTime.Parse("2020-12-10T22:30:51.0724501+00:00"), State = 1, Project = p1, Performer = u1 };
            Task ta7 = new Task { Id = 7, Name = "Quasi consectetur nesciunt doloribus.11", Description = "Praesentium autem consequatur magnam et doloribus exercitationem.\nAut animi fuga cupiditate debitis atque nisi consequatur consequatur.\nCupiditate necessitatibus quo eos sequi earum et quis accusamus.", CreatedAt = DateTime.Parse("2020-06-30T22:22:09.9030937+00:00"), FinishedAt = DateTime.Parse("2022-12-10T22:30:51.0724501+00:00"), State = 2, Project = p1, Performer = u1 };


            Tasks = new List<Task> { ta1, ta2, ta3, ta4, ta5, ta6, ta7 };
        }



    }



}

