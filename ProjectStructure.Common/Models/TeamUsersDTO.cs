﻿using System.Collections.Generic;

namespace ProjectStructure.Common.Models
{
    public class TeamUsersDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserDTO> Users { get; set; }
    }
}
