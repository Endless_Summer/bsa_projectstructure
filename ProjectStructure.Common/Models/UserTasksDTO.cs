﻿using System.Collections.Generic;

namespace ProjectStructure.Common.Models
{
    public class UserTasksDTO
    {
        public UserDTO User { get; set; }
        public List<TaskDTO> Tasks { get; set; }
    }
}
