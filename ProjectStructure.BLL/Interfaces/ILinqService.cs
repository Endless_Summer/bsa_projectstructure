﻿using ProjectStructure.Common.Models;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ILinqService
    {
        IEnumerable<ProjectUserTasksCountDTO> GetProjectUserTasksCount(int userId); // Task 1
        IEnumerable<TaskDTO> GetUserTasks(int userId); // Task 2
        IEnumerable<TaskShortDTO> GetFinishedTasksForUser(int userId); // Task 3
        IEnumerable<TeamUsersDTO> GetAgeLimitTeams(); // TASK 4
        IEnumerable<UserTasksDTO> GetSortedUsers(); // TASK 5
        UserInfoDTO GetUserLastProjectInfo(int userId); // Task 6
        IEnumerable<ProjectShortInfo> GetProjectShortInfo(); // Task 7


    }
}
