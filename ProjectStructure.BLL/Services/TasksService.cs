﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class TasksService : ITasksService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;
        public TasksService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            db = unitOfWork;
            _mapper = mapper;
        }
        public void CreateTask(TaskDTO item)
        {
            item.Id = db.Tasks.Get().Max(u => u.Id) + 1;
            db.Tasks.Create(_mapper.Map<TaskDTO, Task>(item));
        }

        public TaskDTO FindTaskById(int id)
        {
            var entity = db.Tasks.FindById(id);
            return _mapper.Map<Task, TaskDTO>(entity);
        }

        public IEnumerable<TaskDTO> GetTasks()
        {
            var entities = db.Tasks.Get();

            return _mapper.Map<IEnumerable<Task>, List<TaskDTO>>(entities);
        }

        public void RemoveTask(int id)
        {
            var task = db.Tasks.FindById(id);

            if (task == null)
                throw new ArgumentNullException($"Id {id} not found");

            db.Tasks.Remove(task);
        }

        public void UpdateTask(TaskDTO item)
        {
            var task = _mapper.Map<TaskDTO, Task>(item);

            db.Tasks.Update(task);
        }
    }
}
