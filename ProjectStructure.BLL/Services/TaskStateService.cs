﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class TaskStateService : ITaskStateService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;
        public TaskStateService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            db = unitOfWork;
            _mapper = mapper;
        }
        public void CreateTaskState(TaskStateDTO item)
        {
            item.Id = db.TaskStates.Get().Max(u => u.Id) + 1;
            db.TaskStates.Create(_mapper.Map<TaskStateDTO, TaskState>(item));
        }

        public TaskStateDTO FindTaskStateById(int id)
        {
            var entity = db.TaskStates.FindById(id);
            return _mapper.Map<TaskState, TaskStateDTO>(entity);
        }

        public IEnumerable<TaskStateDTO> GetTaskStates()
        {
            var entities = db.TaskStates.Get();

            return _mapper.Map<IEnumerable<TaskState>, List<TaskStateDTO>>(entities);
        }

        public void RemoveTaskState(int id)
        {
            var taskState = db.TaskStates.FindById(id);

            if (taskState == null)
                throw new ArgumentNullException($"Id {id} not found");

            db.TaskStates.Remove(taskState);
        }

        public void UpdateTaskState(TaskStateDTO item)
        {
            var taskState = _mapper.Map<TaskStateDTO, TaskState>(item);

            db.TaskStates.Update(taskState);
        }
    }
}
