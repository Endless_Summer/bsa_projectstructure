﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class TeamsService : ITeamsService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;
        public TeamsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            db = unitOfWork;
            _mapper = mapper;
        }
        public void CreateTeam(TeamDTO item)
        {
            item.Id = db.Teams.Get().Max(u => u.Id) + 1;
            db.Teams.Create(_mapper.Map<TeamDTO, Team>(item));
        }

        public TeamDTO FindTeamById(int id)
        {
            var entity = db.Teams.FindById(id);
            return _mapper.Map<Team, TeamDTO>(entity);
        }

        public IEnumerable<TeamDTO> GetTeams()
        {
            var entities = db.Teams.Get();

            return _mapper.Map<IEnumerable<Team>, List<TeamDTO>>(entities);
        }

        public void RemoveTeam(int id)
        {
            var team = db.Teams.FindById(id);

            if (team == null)
                throw new ArgumentNullException($"Id {id} not found");

            db.Teams.Remove(team);
        }

        public void UpdateTeam(TeamDTO item)
        {
            var team = _mapper.Map<TeamDTO, Team>(item);

            db.Teams.Update(team);
        }
    }
}
