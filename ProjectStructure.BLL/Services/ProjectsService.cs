﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class ProjectsService : IProjectsService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;
        public ProjectsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            db = unitOfWork;
            _mapper = mapper;
        }
        public void CreateProject(ProjectDTO item)
        {
            item.Id = db.Projects.Get().Max(u => u.Id) + 1;
            db.Projects.Create(_mapper.Map<ProjectDTO, Project>(item));
        }

        public ProjectDTO FindProjectById(int id)
        {
            var entity = db.Projects.FindById(id);
            return _mapper.Map<Project, ProjectDTO>(entity);
        }

        public IEnumerable<ProjectDTO> GetProjects()
        {
            var entities = db.Projects.Get();

            return _mapper.Map<IEnumerable<Project>, List<ProjectDTO>>(entities);
        }

        public void RemoveProject(int id)
        {
            var project = db.Projects.FindById(id);

            if (project == null)
                throw new ArgumentNullException($"Id {id} not found");

            db.Projects.Remove(project);
        }

        public void UpdateProject(ProjectDTO item)
        {
            var project = _mapper.Map<ProjectDTO, Project>(item);

            db.Projects.Update(project);
        }
    }
}
