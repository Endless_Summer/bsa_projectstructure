﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;
        public UsersService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            db = unitOfWork;
        }

        public void CreateUser(UserDTO item)
        {
            item.Id = db.Users.Get().Max(u => u.Id) + 1;
            db.Users.Create(_mapper.Map<UserDTO, User>(item));
        }

        public UserDTO FindUserById(int id)
        {
            var entity = db.Users.FindById(id);
            return _mapper.Map<User, UserDTO>(entity);
        }

        public IEnumerable<UserDTO> GetUsers()
        {
            var entities = db.Users.Get();

            return _mapper.Map<IEnumerable<User>, List<UserDTO>>(entities);
        }

        public void RemoveUser(int id)
        {
            var user = db.Users.FindById(id);
            if (user == null)
                throw new ArgumentNullException($"Id {id} not found");

            db.Users.Remove(user);
        }

        public void UpdateUser(UserDTO item)
        {
            var user = _mapper.Map<UserDTO, User>(item);

            db.Users.Update(user);
        }
    }
}
