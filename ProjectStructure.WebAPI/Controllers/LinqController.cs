﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using System.Collections.Generic;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        readonly ILinqService _linqService;
        public LinqController(ILinqService linqService)
        {
            _linqService = linqService;

        }

        // GET: api/Linq/ProjectUserTasksCount/1
        [HttpGet("ProjectUserTasksCount/{id}")] // Task 1
        public ActionResult<IEnumerable<ProjectUserTasksCountDTO>> GetProjectUserTasksCount(int id)
        {
            return Ok(_linqService.GetProjectUserTasksCount(id));
        }

        // GET: api/Linq/UserTasks/1
        [HttpGet("UserTasks/{id}")] // Task 2
        public ActionResult<IEnumerable<TaskDTO>> GetUserTasks(int id)
        {
            return Ok(_linqService.GetUserTasks(id));
        }

        // GET: api/Linq/FinishedTasksForUser/1
        [HttpGet("FinishedTasksForUser/{id}")] // Task 3
        public ActionResult<IEnumerable<TaskShortDTO>> GetFinishedTasksForUser(int id)
        {
            return Ok(_linqService.GetFinishedTasksForUser(id));
        }

        // GET: api/Linq/AgeLimitTeams
        [HttpGet("AgeLimitTeams")] // Task 4
        public ActionResult<IEnumerable<TeamUsersDTO>> GetAgeLimitTeams()
        {
            return Ok(_linqService.GetAgeLimitTeams());
        }

        // GET: api/Linq/SortedUsers
        [HttpGet("SortedUsers")] // Task 5
        public ActionResult<IEnumerable<UserTasksDTO>> GetSortedUsers()
        {
            return Ok(_linqService.GetSortedUsers());
        }


        // GET: api/Linq/UserLastProjectInfo/1
        [HttpGet("UserLastProjectInfo/{id}")] // Task 6
        public ActionResult<UserInfoDTO> GetUserLastProjectInfo(int id)
        {
            return Ok(_linqService.GetUserLastProjectInfo(id));
        }

        // GET: api/Linq/GetProjectShortInfo
        [HttpGet("ProjectShortInfo")] // Task 7
        public ActionResult<IEnumerable<ProjectShortInfo>> GetProjectShortInfo()
        {
            return Ok(_linqService.GetProjectShortInfo());
        }
    }
}
