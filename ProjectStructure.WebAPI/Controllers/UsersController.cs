﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _userService;

        public UsersController(IUsersService userService)
        {
            _userService = userService;
        }

        // GET: api/Users
        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> Get()
        {
            return Ok(_userService.GetUsers());
        }


        // GET api/Users/5
        [HttpGet("{id}")]
        public ActionResult<UserDTO> Get(int id)
        {
            UserDTO result = _userService.FindUserById(id);

            if (result == null)
                return NoContent();

            return Ok(result);
        }


        // POST api/Users
        [HttpPost]
        public ActionResult Post([FromBody] UserDTO value)
        {
            _userService.CreateUser(value);
            return Ok();
        }

        // PUT api/Users/5
        [HttpPut]
        public ActionResult Put([FromBody] UserDTO value)
        {
            try
            {
                _userService.UpdateUser(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/Users/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _userService.RemoveUser(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
            return NoContent();
        }
    }
}
