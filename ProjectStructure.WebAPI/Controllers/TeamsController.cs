﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamsService _teamsService;
        public TeamsController(ITeamsService teamsService)
        {
            _teamsService = teamsService;
        }
        // GET: api/Teams
        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> Get()
        {
            return Ok(_teamsService.GetTeams());
        }

        // GET api/Teams/5
        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            TeamDTO result = _teamsService.FindTeamById(id);

            if (result == null)
                return NoContent();

            return Ok(result);
        }

        // POST api/Teams
        [HttpPost]
        public ActionResult Post([FromBody] TeamDTO value)
        {
            _teamsService.CreateTeam(value);
            return Ok();
        }

        // PUT api/Teams/5
        [HttpPut()]
        public ActionResult Put([FromBody] TeamDTO value)
        {
            try
            {
                _teamsService.UpdateTeam(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/Teams/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _teamsService.RemoveTeam(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
            return NoContent();
        }
    }
}
