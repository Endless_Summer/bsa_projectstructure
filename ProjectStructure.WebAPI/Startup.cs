using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL;
using System;

namespace ProjectStructure.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSingleton<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<IProjectsService, ProjectsService>();
            services.AddSingleton<ITasksService, TasksService>();
            services.AddSingleton<ITaskStateService, TaskStateService>();
            services.AddSingleton<ITeamsService, TeamsService>();
            services.AddSingleton<IUsersService, UsersService>();
            services.AddSingleton<ILinqService, LinqService>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
