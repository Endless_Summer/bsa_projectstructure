﻿using Newtonsoft.Json;
using ProjectStructure.Common.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace ProjectStructure.UI
{
    internal class Menu : IDisposable
    {
        private bool isQuit = false;
        private HttpClient client;
        private bool disposedValue;

        internal void Start(string baseAddress)
        {
            client = new HttpClient
            {
                BaseAddress = new Uri(baseAddress)
            };

            GetMenu();
        }

        private void GetMenu()
        {
            while (!isQuit) // the loop works until we select the action "0 - Quit"
            {
                ShowOptions(); // Displaying elements of necessary actions on the screen

                char choice = Console.ReadKey().KeyChar; // wait until the choice is made and write to the "choice" variable
                Console.WriteLine("\n");

                try
                {
                    switch (choice)
                    {
                        case '1':
                            GetProjectUserTasksCount();
                            break;
                        case '2':
                            GetUserTasks();
                            break;
                        case '3':
                            GetFinishedTasksForUser();
                            break;
                        case '4':
                            GetAgeLimitTeams();
                            break;
                        case '5':
                            GetSortedUsers();
                            break;
                        case '6':
                            GetUserLastProjectInfo();
                            break;
                        case '7':
                            GetProjectShortInfo();
                            break;
                        case '8':
                            CRUDoperationsForUsers();
                            break;
                        case '9':
                            CRUDoperationsForProjects();
                            break;
                        case 'a':
                            CRUDoperationsForTasks();
                            break;
                        case 'b':
                            CRUDoperationsForTaskStates();
                            break;
                        case 'c':
                            CRUDoperationsForTeams();
                            break;
                        case '0':
                            isQuit = true;
                            break; // Quit from menu and programm
                        case 'Q':
                            isQuit = true;
                            break; // Quit from menu and programm
                        case 'q':
                            isQuit = true;
                            break; // Quit from menu and programm
                        default:
                            WriteText("Incorrect enter!", ConsoleColor.Red);
                            break;
                    }

                }
                catch (Exception e)
                {
                    WriteText(e.Message, ConsoleColor.Red);
                }
                finally
                {
                    WriteText("Press any key to continue...", ConsoleColor.Green);

                    Console.ReadKey();
                }
            }
        }

        private void GetProjectUserTasksCount() // Task 1
        {
            int id = GetId();

            string response = client.GetStringAsync($"/api/Linq/ProjectUserTasksCount/{id}").Result;
            var result = JsonConvert.DeserializeObject<List<ProjectUserTasksCountDTO>>(response);

            foreach (var item in result)
            {
                Console.WriteLine($"Project ID: {item.Project.Id} Number of tasks: {item.UserTasksCount}");
            }
            Console.WriteLine();

        }
        private void GetUserTasks() // Task 2
        {
            int id = GetId();

            string response = client.GetStringAsync($"/api/Linq/UserTasks/{id}").Result;
            var result = JsonConvert.DeserializeObject<List<TaskDTO>>(response);


            if (result.Count == 0)
                Console.WriteLine("No tasks that satisfy the condition were found");

            foreach (var task in result)
            {
                Console.WriteLine($"Task ID: {task.Id} Name: {task.Name} (length = {task.Name.Length})");
            }
            Console.WriteLine();
        }
        private void GetFinishedTasksForUser() // Task 3
        {
            int id = GetId();
            string response = client.GetStringAsync($"/api/Linq/FinishedTasksForUser/{id}").Result;
            var result = JsonConvert.DeserializeObject<List<TaskShortDTO>>(response);

            if (result.Count == 0)
                Console.WriteLine("No tasks that satisfy the condition were found");

            foreach (var task in result)
            {
                Console.WriteLine($"Task Id: {task.Id} Name: {task.Name}");
            }

            Console.WriteLine();
        }

        private void GetAgeLimitTeams() // Task 4
        {
            string response = client.GetStringAsync($"/api/Linq/AgeLimitTeams").Result;
            var result = JsonConvert.DeserializeObject<List<TeamUsersDTO>>(response);

            foreach (var team in result)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Project ID: {team.Id} Name: {team.Name}");
                Console.ResetColor();
                Console.WriteLine("MEMBERS:");
                foreach (var member in team.Users)
                {
                    Console.WriteLine($"Member ID: {member.Id}\tName: {member.FirstName} {member.LastName}\tRegisteredAt: {member.RegisteredAt}\tYear of birthday: {member.Birthday.Year}");
                }

                Console.WriteLine("=======================================");
            }
        }

        private void GetSortedUsers() // Tesk 5
        {
            string response = client.GetStringAsync($"/api/Linq/SortedUsers").Result;
            var result = JsonConvert.DeserializeObject<List<UserTasksDTO>>(response);

            foreach (var user in result)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"User ID: {user.User.Id} Name: {user.User.FirstName}");
                Console.ResetColor();
                Console.WriteLine("TASKS:");

                foreach (var task in user.Tasks)
                {
                    Console.WriteLine($"Task ID: {task.Id} Name: {task.Name}");
                }

                Console.WriteLine("=======================================");
            }
        }
        private void GetUserLastProjectInfo() // Task 6
        {
            int id = GetId();
            string response = client.GetStringAsync($"/api/Linq/UserLastProjectInfo/{id}").Result;
            var result = JsonConvert.DeserializeObject<UserInfoDTO>(response);

            Console.WriteLine($"User ID: {result.User.Id} " +
                $"Name: {result.User.FirstName} {result.User.LastName}");
            Console.WriteLine($"Last user project: ID: {result.LastProject.Id} Name: {result.LastProject.Name}");
            Console.WriteLine($"Number of tasks in last user project: {result.TasksLastProject}");
            Console.WriteLine($"Number of not completed and canceled tasks: {result.NotComletedTasks}");
            Console.WriteLine($"The longest task: TaskID: {result.MaxTask?.Id.ToString() ?? "There are no tasks for this user"} Created: {result.MaxTask?.CreatedAt.Date} Finished: {result.MaxTask?.FinishedAt.Date}");
        }

        private void GetProjectShortInfo() // Yask 7
        {
            string response = client.GetStringAsync($"/api/Linq/ProjectShortInfo").Result;
            var result = JsonConvert.DeserializeObject<List<ProjectShortInfo>>(response);

            foreach (var p in result)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Project: {p.Project.Id} {p.Project.Name}");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"The longest project task (by description):");
                Console.ResetColor();
                Console.WriteLine($"Task ID: {p.LongDescriptionTask?.Id} Task description: {p.LongDescriptionTask?.Description}");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"The shortest project task (by name):");
                Console.ResetColor();
                Console.WriteLine($"Task ID: {p.ShortestNameTask?.Id} Task name: {p.ShortestNameTask?.Name}");
                WriteText("Users in project team: ", ConsoleColor.Blue, false);
                Console.WriteLine($"{p.UserCount}");

                Console.WriteLine("=======================================");
            }

        }

        private void ShowOptions()
        {
            Console.Clear();

            WriteText("╔═════════════════════════════════╗", ConsoleColor.Blue);
            WriteText("║ Welcome to Projects Repository! ║", ConsoleColor.Blue);
            WriteText("║ What do you want to do?         ║", ConsoleColor.Blue);
            WriteText("╚═════════════════════════════════╝", ConsoleColor.Blue);
            Console.WriteLine();


            WriteText("1", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати кiлькiсть таскiв у проектi конкретного користувача (по id)");

            WriteText("2", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати список таскiв, призначених для конкретного користувача (по id),\n    де name таска <45 символiв (колекцiя з таскiв)");

            WriteText("3", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати список (id, name) з колекцiї таскiв, якi виконанi (finished)\n    в поточному (2020) роцi для конкретного користувача (по id)");

            WriteText("4", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати список (id, iм'я команди i список користувачiв) з колекцiї команд,\n    учасники яких старшi 10 рокiв, вiдсортованих за датою реєстрацiї\n    користувача за спаданням, а також згрупованих по командах.");

            WriteText("5", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати список користувачiв за алфавiтом first_name (по зростанню)\n    з вiдсортованими tasks по довжинi name (за спаданням)");

            WriteText("6", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати задану структуру (передати Id користувача в параметри)");

            WriteText("7", ConsoleColor.Green, false);
            Console.WriteLine(" - Отримати таку структуру: Проект, Найдовший таск проекту (за описом),\n    Найкоротший таск проекту (по iменi), Загальна кiлькiсть користувачiв в\n    командi проекту, де або опис проекту >20 символiв, або кiлькiсть таскiв <3");

            WriteText("8", ConsoleColor.Green, false);
            Console.WriteLine(" - Тестування Create/Read/Update/Delete для User");

            WriteText("9", ConsoleColor.Green, false);
            Console.WriteLine(" - Тестування Create/Read/Update/Delete для Project");

            WriteText("a", ConsoleColor.Green, false);
            Console.WriteLine(" - Тестування Create/Read/Update/Delete для Task");

            WriteText("b", ConsoleColor.Green, false);
            Console.WriteLine(" - Тестування Create/Read/Update/Delete для TaskState");

            WriteText("c", ConsoleColor.Green, false);
            Console.WriteLine(" - Тестування Create/Read/Update/Delete для Team");


            WriteText("Q", ConsoleColor.Green, false);
            Console.WriteLine(" - Quit");

            Console.Write("\nSelect option: ");
        }

        private void WriteText(string text, ConsoleColor color, bool isLine = true)
        {
            Console.ForegroundColor = color;

            if (isLine)
                Console.WriteLine(text);
            else
                Console.Write(text);

            Console.ResetColor();
        }

        private int GetId(string str = "Enter User Id: ")
        {
            int inputText;
            do
            {
                Console.WriteLine(str);
            } while (!int.TryParse(Console.ReadLine(), out inputText));

            return inputText;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects)
                }
                client.Dispose();
                disposedValue = true;
            }
        }
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        private void CRUDoperationsForUsers()
        {
            // Create
            WriteText("Create new user:", ConsoleColor.Blue);
            var entity = new UserDTO() { Id = 77, FirstName = "FirstName", LastName = "LastName", Email = "qq@qq.com", Birthday = DateTime.Now, RegisteredAt = DateTime.Now, TeamId = 2 };

            string jsonInString = JsonConvert.SerializeObject(entity);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var response = client.PostAsync(@"/api/Users", stringContent).Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Delete
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Delete user ID 4:", ConsoleColor.Blue);

            response = client.DeleteAsync(@"/api/Users/4").Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Update
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Update user ID 3:", ConsoleColor.Blue);

            entity = new UserDTO() { Id = 3, FirstName = "Name333", LastName = "Name333", Email = "qq333@qq.com", Birthday = DateTime.Now, RegisteredAt = DateTime.Now, TeamId = 1 };
            jsonInString = JsonConvert.SerializeObject(entity);
            stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            response = client.PutAsync(@"/api/Users", stringContent).Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Read by ID
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Read user ID 3:", ConsoleColor.Blue);

            string responseStr = client.GetStringAsync(@"/api/Users/3").Result;
            entity = JsonConvert.DeserializeObject<UserDTO>(responseStr);

            Console.WriteLine($"Id: {entity.Id}, FirstName: {entity.FirstName}, LastName: {entity.LastName}");

            //Read All
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Read all users:", ConsoleColor.Blue);

            responseStr = client.GetStringAsync(@"/api/Users").Result;
            List<UserDTO> users = JsonConvert.DeserializeObject<List<UserDTO>>(responseStr);
            foreach (var item in users)
            {
                Console.WriteLine($"Id: {item.Id}, FirstName: {item.FirstName}, LastName: {item.LastName}");
            }

        }
        private void CRUDoperationsForProjects()
        {
            // Create
            WriteText("Create new project:", ConsoleColor.Blue);
            var entity = new ProjectDTO() { Id = 77, Name = "Name", AuthorId = 1, TeamId = 1, CreatedAt = DateTime.Now, Deadline = DateTime.Now, Description = "Description" };

            string jsonInString = JsonConvert.SerializeObject(entity);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var response = client.PostAsync(@"/api/Projects", stringContent).Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Delete
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Delete project ID 2:", ConsoleColor.Blue);

            response = client.DeleteAsync(@"/api/Projects/2").Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Update
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Update Project ID 3:", ConsoleColor.Blue);

            entity = new ProjectDTO() { Id = 3, Name = "Name333", Description = "Description333", AuthorId = 1, TeamId = 1, CreatedAt = DateTime.Now, Deadline = DateTime.Now };
            jsonInString = JsonConvert.SerializeObject(entity);
            stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            response = client.PutAsync(@"/api/Projects", stringContent).Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Read by ID
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Read Project ID 3:", ConsoleColor.Blue);

            string responseStr = client.GetStringAsync(@"/api/Projects/3").Result;
            entity = JsonConvert.DeserializeObject<ProjectDTO>(responseStr);

            Console.WriteLine($"Id: {entity.Id}, Name: {entity.Name}, Description: {entity.Description}");

            //Read All
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Read all Projects:", ConsoleColor.Blue);

            responseStr = client.GetStringAsync(@"/api/Projects").Result;
            var entities = JsonConvert.DeserializeObject<List<ProjectDTO>>(responseStr);
            foreach (var item in entities)
            {
                Console.WriteLine($"Id: {item.Id}, Name: {item.Name}, Description: {item.Description}");
            }

        }

        private void CRUDoperationsForTeams()
        {
            // Create
            WriteText("Create new Team:", ConsoleColor.Blue);
            var entity = new TeamDTO() { Id = 77, Name = "Name", CreatedAt = DateTime.Now };

            string jsonInString = JsonConvert.SerializeObject(entity);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var response = client.PostAsync(@"/api/Teams", stringContent).Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Delete
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Delete Team ID 2:", ConsoleColor.Blue);

            response = client.DeleteAsync(@"/api/Teams/2").Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Update
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Update Team ID 3:", ConsoleColor.Blue);

            entity = new TeamDTO() { Id = 3, Name = "Name333", CreatedAt = DateTime.Now};
            jsonInString = JsonConvert.SerializeObject(entity);
            stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            response = client.PutAsync(@"/api/Teams", stringContent).Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Read by ID
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Read Team ID 3:", ConsoleColor.Blue);

            string responseStr = client.GetStringAsync(@"/api/Teams/3").Result;
            entity = JsonConvert.DeserializeObject<TeamDTO>(responseStr);

            Console.WriteLine($"Id: {entity.Id}, Name: {entity.Name}, CreatedAt: {entity.CreatedAt}");

            //Read All
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Read all Teams:", ConsoleColor.Blue);

            responseStr = client.GetStringAsync(@"/api/Teams").Result;
            var entities = JsonConvert.DeserializeObject<List<TeamDTO>>(responseStr);
            foreach (var item in entities)
            {
                Console.WriteLine($"Id: {item.Id}, Name: {item.Name}, CreatedAt: {item.CreatedAt}");
            }
        }

        private void CRUDoperationsForTaskStates()
        {
            // Create
            WriteText("Create new TaskState:", ConsoleColor.Blue);
            var entity = new TaskStateDTO() { Id = 77, Value = "NewState"};

            string jsonInString = JsonConvert.SerializeObject(entity);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var response = client.PostAsync(@"/api/TaskStates", stringContent).Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Delete
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Delete TaskState ID 2:", ConsoleColor.Blue);

            response = client.DeleteAsync(@"/api/TaskStates/2").Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Update
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Update TaskState ID 3:", ConsoleColor.Blue);

            entity = new TaskStateDTO() { Id = 3, Value = "UpdateState" };
            jsonInString = JsonConvert.SerializeObject(entity);
            stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            response = client.PutAsync(@"/api/TaskStates", stringContent).Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Read by ID
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Read TaskState ID 3:", ConsoleColor.Blue);

            string responseStr = client.GetStringAsync(@"/api/TaskStates/3").Result;
            entity = JsonConvert.DeserializeObject<TaskStateDTO>(responseStr);

            Console.WriteLine($"Id: {entity.Id}, Value: {entity.Value}");

            //Read All
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Read all TaskStates:", ConsoleColor.Blue);

            responseStr = client.GetStringAsync(@"/api/TaskStates").Result;
            var entities = JsonConvert.DeserializeObject<List<TaskStateDTO>>(responseStr);
            foreach (var item in entities)
            {
                Console.WriteLine($"Id: {item.Id}, Value: {item.Value}");
            }
        }

        private void CRUDoperationsForTasks()
        {
            // Create
            WriteText("Create new Task:", ConsoleColor.Blue);
            var entity = new TaskDTO() { Id = 77, Name = "Name", CreatedAt = DateTime.Now, Description = "Description", FinishedAt = DateTime.Now, PerformerId = 1, ProjectId = 1, State = 2 };

            string jsonInString = JsonConvert.SerializeObject(entity);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var response = client.PostAsync(@"/api/Tasks", stringContent).Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Delete
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Delete Task ID 2:", ConsoleColor.Blue);

            response = client.DeleteAsync(@"/api/Tasks/2").Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Update
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Update Task ID 3:", ConsoleColor.Blue);

            entity = new TaskDTO() { Id = 3, Name = "Name333", CreatedAt = DateTime.Now, Description = "Description333", FinishedAt = DateTime.Now, PerformerId = 1, ProjectId = 1, State = 2 };
            jsonInString = JsonConvert.SerializeObject(entity);
            stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            response = client.PutAsync(@"/api/Tasks", stringContent).Result;

            if (response.IsSuccessStatusCode)
                WriteText($"Status code is success {response.StatusCode}!", ConsoleColor.Green);
            else
                Console.WriteLine($"Status code is fail {response.StatusCode}!", ConsoleColor.Red);

            // Read by ID
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Read Task ID 3:", ConsoleColor.Blue);

            string responseStr = client.GetStringAsync(@"/api/Tasks/3").Result;
            entity = JsonConvert.DeserializeObject<TaskDTO>(responseStr);

            Console.WriteLine($"Id: {entity.Id}, Name: {entity.Name}, Description: {entity.Description}");

            //Read All
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            WriteText("Read all Projects:", ConsoleColor.Blue);

            responseStr = client.GetStringAsync(@"/api/Tasks").Result;
            var entities = JsonConvert.DeserializeObject<List<TaskDTO>>(responseStr);
            foreach (var item in entities)
            {
                Console.WriteLine($"Id: {item.Id}, Name: {item.Name}, Description: {item.Description}");
            }
        }




    }
}
