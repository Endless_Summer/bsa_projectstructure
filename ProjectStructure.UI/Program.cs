﻿using Microsoft.Extensions.Configuration;
using System;

namespace ProjectStructure.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var configuration = new ConfigurationBuilder()
                    .AddJsonFile(@$"appsettings.json")
                    .Build();
                string baseAddress = configuration["BaseAddress"];

                using Menu menu = new Menu();
                menu.Start(baseAddress);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }

        }

    }
}

